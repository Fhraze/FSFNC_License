# Features:
| ✓ You can                         | ✗ You cannot                | ﹗ You must             |
| --------------------------------- | --------------------------- | ----------------------- |
| • Use privately                   | • Use commercially          | • Give credit           |
| • Use non-commercially            | • Use to train AI           | • Indicate changes made |
| • Use close-sourcing licenses     | • Use for law enforcement   | • Include the license   |
| • Distribute                      | • Use for military purposes |                         |
| • Use a patent (with limitations) | • Use for bigoted purposes  |                         |
| • Add a warranty                  | • Use for violent purposes  |                         |
|                                   | • Monetize it in any way    |                         |
|                                   | • Hold the licensor liable  |                         |


<div align="center">

# License:
<< https://fsfncl.tiiny.site/ >>
</div>

```
			Frey's Sentiently Free Non-Commercial License
					Version 1, 20 June 2024

Copyright (c) <year the license was applied> <author>

START OF DEFINITION OF THE LANGUAGE USED IN THIS LICENSE . . .

 !. The license is written in the English language of planet Earth, as it was
    spoken in the United States of America (USA) at the time this license was
    created.

 1. SENTIENT: is defined, in this license, as a self-conscious being capable
              of feeling real and genuine emotions.
              Example: a regular human (𝘏𝘰𝘮𝘰 𝘴𝘢𝘱𝘪𝘦𝘯𝘴) on Earth.

 2. CREATIVE PIECE: is defined, in this license, as any kind of work, artistic
                    or not, abstract or concrete, made by sentient beings.

 3. SOURCE CODE: is defined, in this license, as all the:
                  - text (usually plain text) that conforms to a human-readable
                    programming language and specifies the behavior of a
                    computer;
                  - any and all content utilized in the making of the creative
                    piece.

. . . END OF DEFINITION

START OF *MAIN* PERMISSIONS AND RESTRICTIONS . . .

 1. USE: any SENTIENT being is allowed to use this piece as long as it is
         not for military, violent, bigoted, or law enforcement purposes.
         But when it comes to companies or any kind of organization, guarantee
         is required that the piece is and will only be used by sentient
         beings, such as humans.

 2. COMMERCIAL USE: as stated in the first permission, any sentient being is
                    allowed to use. Although, when it comes to commercial use,
                    there are some limitations:
                     - usage in the means of production of commercial content
                       is allowed;
                     - usage *as* part of the commercial content is NOT allowed.

 3. DISTRIBUTION: anyone, anything or any kind of organization is freely allowed
                  to distribute and redistribute this pieace as long as this
                  license keeps attached and in effect over it's creative piece.

 4. PATENTING: when patenting this creative piece or any piece that uses any
               portion of it, the patenting license's conditions must not
               conflict with the ones imposed by this license. The conditions
               of the "Frey's Sentiently Free Non-Commercial License" should
               prevail over any other.

 5. MODIFICATION: any SENTIENT being is allowed to modify and adapt this piece
                  in every possible way. Although you MUST make all of your
                  changes clear and visible to the public.

. . . END OF *MAIN* PERMISSIONS AND RESTRICTIONS

START OF *SECONDARY* PERMISSIONS AND RESTRICTIONS . . .

 !. All conditions stated inside this secondary permissions and restrictions
    section are valid only if no more licenses are assigned to the same
    creative piece other than the FSFNC License.

 1. it is also mandatory, if the creative piece this license is assigned to
    is any kind of software or work that contains a source code, that the full
    source code is made available publicly

. . . END OF *SECONDARY* PERMISSIONS AND RESTRICTIONS

This license, the above copyright notice, language definitions, permissions
and restrictions (both MAIN and SECONDARY) shall be included in all copies or
substatial portions of the creative piece it's assined to, giving credit to
it's original creator.

This license allows for other licenses to be applied to the same creative piece,
as long as their conditions don't conflict in any way with the conditions
stated in the *MAIN* permissions and restrictions section of this license.

THIS CREATIVE PIECE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE CREATIVE PIECE OR THE USE OR OTHER DEALINGS
IN THE PIECE.
```